<h1>About project</h1>

___________________________________________________________________________________

<p>Php-routing it's just a php project which you can clone with this git command</p>

`git clone https://gitlab.com/rubyqorn/php-routing.git`

<p> and to test what i did. It's a simple routing system, which you can understand if you will spend 30 min.</p>

<h2>Main directories</h2>

<p>All main directories contains in src dir. There you can find subdirectories:</p>

<ul>
	<li>Config</li>
	<li>Controllers</li>
	<li>Http</li>
	<li>Resources</li>
</ul>

<h3>Config directory</h3>

<p>It's a directory where you can find routes file. In this file contains all routes which you will use in your web application. Also, in this directory you can configure all files which have relate for your web application</p>

<h3>Controllers directory</h3>

<p>In this directory i have create example controller which you can use for testing my web application. If you want you can delete this example controller and create own which will be solve your problems.</p>

<h3>Http directory</h3>

<p>This is main directory where contains classes for working with routing. You can customize them how you want. But you have to memory about that all classes and iterfaces interconnected</p>

<h4>Resources</h4>

<p>There you can find simple templates for errors and welcome page.</p>