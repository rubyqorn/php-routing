<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Rubyqorn\\Controllers\\ExampleController' => $baseDir . '/src/Controllers/ExampleController.php',
    'Rubyqorn\\Debuger' => $baseDir . '/src/Debuger.php',
    'Rubyqorn\\Http\\CustomErrors' => $baseDir . '/src/Http/CustomErrors.php',
    'Rubyqorn\\Http\\Interfaces\\RequestMethods' => $baseDir . '/src/Http/Interfaces/RequestMethods.php',
    'Rubyqorn\\Http\\Interfaces\\RouteMethods' => $baseDir . '/src/Http/Interfaces/RouteMethods.php',
    'Rubyqorn\\Http\\Request' => $baseDir . '/src/Http/Request.php',
    'Rubyqorn\\Http\\Route' => $baseDir . '/src/Http/Route.php',
    'Rubyqorn\\Http\\RouteParser' => $baseDir . '/src/Http/RouteParser.php',
);
