<?php 
namespace Rubyqorn\Http;

use Rubyqorn\Debuger;
use Rubyqorn\Http\{
	Request,
	CustomErrors
};

class RouteParser
{
	/**
	* @var string
	*/ 
	private static $url = null;

	/**
	* @var string
	*/ 
	private static $controller;

	/**
	* @var string
	*/ 
	private static $action;


	/**
	* This construct method acts as main method
	*/ 
	public static function run($url, string $controller, string $action)
	{
		return self::routeHandler($url, $controller, $action);
	}

	/**
	* @return Work with route from routes file
	* and url from request line
	*/ 
	private function routeHandler($url, string $controller, string $action)
	{
		$parsedUrl = self::parse(Request::get('url'));
		$parsedRoute = explode('/', trim($url, '/'));

		if ($parsedUrl == $parsedRoute) {
			self::$controller = 'Rubyqorn\Controllers\\' . ucfirst($controller);
			self::$controller = new self::$controller;
			
			if (isset($action)) {
				self::$action = $action;
			}

			call_user_func([self::$controller, self::$action]);
		} else {
			CustomErrors::error404Page();
		}
	}

	/**
	* @return Parsed url from request line
	*/ 
	private function parse($url)
	{
		self::$url = $url;

		if (isset(self::$url)) {
			return explode('/', rtrim(self::$url, '/'));
		}
		
	}
}