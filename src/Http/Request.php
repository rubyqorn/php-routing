<?php 
namespace Rubyqorn\Http;

use Rubyqorn\Http\Interfaces\RequestMethods;

class Request implements RequestMethods
{

	/**
	* @var array
	*/ 
	private static $superGlobalArray = null;

	/**
	* @return super global array $_GET 
	*
	* @param string $params
	*/ 
	public static function get($params = [])
	{
		return self::$superGlobalArray = $_GET[$params];
	}

	/**
	* @return super global array $_POST 
	*
	* @param string $params
	*/ 
	public static function post($params = [])
	{
		return self::$superGlobalArray = $_POST[$params];
	}

	/**
	* @return super global array $_SERVER 
	*
	* @param string $params
	*/ 
	public static function server($params = [])
	{
		return self::$superGlobalArray = $_SERVER[$params];
	}
}