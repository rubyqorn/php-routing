<?php 
namespace Rubyqorn\Http;

class CustomErrors
{
	/**
	* @return 404 error page
	*/ 
	public static function error404Page()
	{
		require_once '../src/Resources/Errors/404.php';
	} 

	/**
	* @return 503 error page
	*/ 
	public static function error503Page()
	{
		require_once '../src/Resources/Errors/503.php';
	} 
}