<?php 
namespace Rubyqorn\Http;

use Rubyqorn\Http\Interfaces\RouteMethods;
use Rubyqorn\Debuger;
use Rubyqorn\Http\{
	RouteParser,
	Request,
	CustomErrors
};

class Route implements RouteMethods
{
	/**
	* @return Route with GET http method
	*/ 
	public static function get($url, string $controller, string $action)
	{
		if (Request::server('REQUEST_METHOD') == 'GET') {
			return RouteParser::run($url, $controller, $action);
		} else {
			CustomErrors::error503Page();
		}
	}

	/**
	* @return Route with POST http method
	*/ 
	public static function post($url, string $controller, string $action)
	{
		if (Request::server('REQUEST_METHOD') == 'POST') {
			return RouteParser::run($url, $controller, $action);
		} else {
			CustomErrors::error503Page();
		}
	}

	/**
	* @return Route with PUT http method
	*/ 
	public static function put($url, string $controller, string $action)
	{
		if (Request::server('REQUEST_METHOD') == 'PUT') {
			return RouteParser::run($url, $controller, $action);
		} else {
			CustomErrors::error503Page();
		}
	}

	/**
	* @return Route with DELETE http method
	*/ 
	public static function delete($url, string $controller, string $action)
	{
		if (Request::server('REQUEST_METHOD') == 'DELETE') {
			return RouteParser::run($url, $controller, $action);
		} else {
			CustomErrors::error503Page();
		}
	}
}