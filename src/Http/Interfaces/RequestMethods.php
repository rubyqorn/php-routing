<?php 
namespace Rubyqorn\Http\Interfaces;

interface RequestMethods 
{
	/**
	* Return super global array $_GET
	*
	* @param string $params
	*/ 
	public static function get($params = []);

	/**
	* Return super global array $_POST
	*
	* @param string $params
	*/ 
	public static function post($params = []);

	/**
	* Return super globall array $_SERVER 
	*
	* @param string $params
	*/ 
	public static function server($params = []);
}