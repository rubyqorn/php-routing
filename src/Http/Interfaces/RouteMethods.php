<?php 
namespace Rubyqorn\Http\Interfaces;

interface RouteMethods
{
	/**
	* Route with GET http method  
	*
	* @param mixed $url
	* @param string $controller
	* @param string $action
	*/
	public static function get($url, string $controller, string $action); 

	/**
	* Route with POST http method
	*
	* @param mixed $url
	* @param string $controller
	* @param string $action
	*/
	public static function post($url, string $controller, string $action);

	/**
	* Route with PUT http method
	*
	* @param mixed $url
	* @param string $controller
	* @param string $action
	*/ 
	public static function put($url, string $controller, string $action);

	/**
	* Route with DELETE http method
	*
	* @param mixed $url
	* @param string $controller
	* @param string $action
	*/ 
	public static function delete($url, string $controller, string $action);
}