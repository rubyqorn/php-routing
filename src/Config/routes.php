<?php 

/*
*+--------------------------------------------------+
*| There you can find and register all your routes,	|
*| which will be using on your web application		|
*+--------------------------------------------------+
*/ 

use Rubyqorn\Http\Route;

Route::get('/home', 'ExampleController', 'index');