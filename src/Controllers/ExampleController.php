<?php 
namespace Rubyqorn\Controllers;

class ExampleController
{
	/**
	* @return string
	*
	* This is just example method for 
	* testing routes
	*/ 
	public function index()
	{
		require '../src/Resources/welcome.php';
	}

	public function catalog()
	{
		echo 'Catalog page';
	}
}