<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>404</title>
	<link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="/css/app.css">
</head>
<body>

	<section id="error">
		<div class="container">
			<h1 class="error-message">404 | Page not found</h1>
		</div>
	</section>
	
</body>
</html>