<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Rubyqorn Routing</title>
	<link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="/css/app.css">
</head>
<body>

	<section id="intro">
		<div class="container">
			<h1 class="intro-text">Welcome to the Rubyqorn Routing</h1>
			<hr class="slim">
		</div>
	</section>
	
</body>
</html>