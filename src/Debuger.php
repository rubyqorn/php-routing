<?php 
namespace Rubyqorn;

class Debuger
{
	/**
	* @param string $str
	* @return mixed
	*/ 
	public static function debug($str)
	{
		echo '<pre>';
		var_dump($str);
		echo '</pre>';
	}
}